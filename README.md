# iMedia24 Coding challenge


# Usage



## Build and Run the App
Build the project with gradle:
```
gradlew build
```
Run the App as a spring boot application :-
```
gradlew bootRun
```
## Build and Run with Docker

Build Docker OCI Image and run it with docker
```
gradlew bootBuildImage
docker run -p 3080:8080 docker.io/library/shop:0.0.1-SNAPSHOT
```
The app will start running at http://localhost:3080/
