package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.springframework.http.MediaType
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigDecimal
import java.time.ZonedDateTime


@SpringBootTest
@AutoConfigureMockMvc
internal class ProductControllerTest {

    @Autowired lateinit var mockMvc: MockMvc
    @Autowired lateinit var productService: ProductService
    @Autowired lateinit var productRepository: ProductRepository

    @Test
    fun `@GET product by sku route should return an not found if the product does not exist`() {
        mockMvc.perform(MockMvcRequestBuilders.get("/products/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    fun `@GET product by sku route should return a product if the product exists`() {


        val product = ProductEntity("123", "product1", "description1", BigDecimal(123),10, ZonedDateTime.now(),
            ZonedDateTime.now())

        val createdProduct: ProductResponse = productService.addProduct(product)

        mockMvc.perform(MockMvcRequestBuilders.get("/products/" + createdProduct.sku))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                MockMvcResultMatchers.content()
                    .json("{\"sku\":\""+createdProduct.sku+"\"," +
                            "\"name\":\""+createdProduct.name+"\"," +
                            "\"description\":\""+createdProduct.description+"\"," +
                            "\"price\":"+createdProduct.price+"," +
                            "\"stock\":"+createdProduct.stock+"}")
            )

        productRepository.delete(product)
    }

    @Test
    fun `@REQUEST products by skus route should return an empty list if no products exist`() {
        mockMvc.perform(MockMvcRequestBuilders.get("/products?skus=123,4567,8901,2345,67789"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                MockMvcResultMatchers.content()
                    .json("[]")
            )
    }

    @Test
    fun `@GET products by skus route should return only the existing products where the skus are passed in the route `() {

        val firstProduct = ProductEntity("123", "product1", "description1", BigDecimal(123),10, ZonedDateTime.now(),
            ZonedDateTime.now())
        val secondProduct = ProductEntity("4567", "product2", "description2", BigDecimal(4567),9, ZonedDateTime.now(),
            ZonedDateTime.now())
        val thirdProduct = ProductEntity("8901", "product3", "description3", BigDecimal(8901),8, ZonedDateTime.now(),
            ZonedDateTime.now())

        productService.addProduct(firstProduct)
        val createdsecondProduct: ProductResponse = productService.addProduct(secondProduct)
        val createdthirdProduct: ProductResponse = productService.addProduct(thirdProduct)

        mockMvc.perform(MockMvcRequestBuilders.get("/products?skus="+createdsecondProduct.sku+","+createdthirdProduct.sku+",2345,67789"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                MockMvcResultMatchers.content()
                    .json("[{\"headers\":{},\"body\":{\"sku\":\""+createdsecondProduct.sku+"\",\"name\":\""+createdsecondProduct.name+"\",\"description\":\""+createdsecondProduct.description+"\",\"price\":"+createdsecondProduct.price+",\"stock\":"+createdsecondProduct.stock+"},\"status_code\":\"OK\",\"status_code_value\":200},{\"headers\":{},\"body\":{\"sku\":\""+createdthirdProduct.sku+"\",\"name\":\""+createdthirdProduct.name+"\",\"description\":\""+createdthirdProduct.description+"\",\"price\":"+createdthirdProduct.price+",\"stock\":"+createdthirdProduct.stock+"},\"status_code\":\"OK\",\"status_code_value\":200}]")
            )
        productRepository.delete(firstProduct)
        productRepository.delete(secondProduct)
        productRepository.delete(thirdProduct)

    }

    @Test
    fun `@PATCH update product should return a bad request if the body is missing`() {
        mockMvc.perform(MockMvcRequestBuilders.patch("/123"))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test
    fun `@PATCH update product should return not found if the product doesnt exist and the request has a body`() {
        mockMvc.perform(
            MockMvcRequestBuilders.patch("/123")
                .contentType(
                    MediaType.APPLICATION_JSON
                )
                .content(
                         "{\"name\":\"updatedName\"," +
                        "\"description\":\"updatedDescription\"," +
                        "\"price\":500}")
        )
            .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    fun `@PATCH update product should update the product with the given sku when all the parameters good`() {

        val product = ProductEntity("123", "product1", "description1", BigDecimal(123),10, ZonedDateTime.now(),
            ZonedDateTime.now())
        val createdProduct: ProductResponse = productService.addProduct(product)
        val updatedName  = "updatedName"
        val updatedDescription = "updatedDescription"
        val updatedPrice  = BigDecimal(500)

        mockMvc.perform(
            MockMvcRequestBuilders.patch("/123")
                .contentType(
                    MediaType.APPLICATION_JSON
                )
                .content(
                    "{\"name\":\""+updatedName+"\"," +
                            "\"description\":\""+updatedDescription+"\"," +
                            "\"price\":"+updatedPrice+"}")
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                MockMvcResultMatchers.content()
                    .json("{\"sku\":\""+createdProduct.sku+"\"," +
                            "\"name\":\""+updatedName+"\"," +
                            "\"description\":\""+updatedDescription+"\"," +
                            "\"price\":"+updatedPrice+"," +
                            "\"stock\":"+createdProduct.stock+"}")
            )

        productRepository.delete(product)
    }
}