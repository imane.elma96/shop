package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.time.ZonedDateTime
import kotlin.reflect.jvm.internal.ReflectProperties.Val

@Service

class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse?
    {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun findProductBySkus(skus: Iterable <String>): Iterable <ProductResponse>?
    {

        val productResponses: MutableList<ProductResponse> =  mutableListOf()

        skus.forEach{

           findProductBySku(it)?.let { it1 -> productResponses.add(it1) }
        }
        return  productResponses
    }

    fun addProduct(product: ProductEntity): ProductResponse {
        return productRepository.save(product).toProductResponse()
    }

    fun updateProduct(sku: String, name :String, description : String?, price : BigDecimal): ProductResponse ? =


        productRepository.findBySku(sku)?.let{ it->  val updatedTask: ProductEntity =
            it.copy(
                name = name,
                description = description,
                price = price,
                updatedAt = ZonedDateTime.now()
                )
            addProduct(updatedTask)

        }




}
