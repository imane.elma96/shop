package de.imedia24.shop.db.entity

import java.math.BigDecimal

data class UpdateProduct
    (
    val name: String,
    val description: String,
    val price: BigDecimal
)