package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.UpdateProduct
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {

        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)

        return if(product == null) {

            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
        
    }


    @GetMapping(value= ["/products"], produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(@RequestParam skus: List <String>): List<ResponseEntity<ProductResponse>>
    {

        return productService.findProductBySkus(skus)!!.map { product ->
            ResponseEntity.ok(product)
        }
    }

    @PostMapping
    fun addProduct(@RequestBody product: ProductEntity): ResponseEntity<ProductResponse> {

        return ResponseEntity.ok(productService.addProduct(product))

    }

    @PatchMapping("{sku}")
    fun updateProduct(@PathVariable sku: String, @RequestBody product: UpdateProduct): ResponseEntity<ProductResponse>  {

        val updatedProduct = productService.updateProduct(sku, product.name, product.description, product.price)

        return if(updatedProduct == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(updatedProduct)
        }

    }

}
